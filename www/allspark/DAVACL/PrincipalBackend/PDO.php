<?php

namespace Allspark\DAVACL\PrincipalBackend;

use Sabre\DAV;
use Sabre\DAV\MkCol;
use Sabre\Uri;

class PDO extends \Sabre\DAVACL\PrincipalBackend\AbstractBackend
{
	protected $pdo;
	protected $fieldMap = ['{DAV:}displayname' => ['dbField' => 'email'],'{http://sabredav.org/ns}email-address' => ['dbField' => 'email']];

	function __construct(\PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	function getPrincipalsByPrefix($prefixPath)
	{
		$fields = ['CONCAT("principals/",email) as uri'];

		foreach ($this->fieldMap as $key => $value)
			$fields[] = $value['dbField'];

		$result = $this->pdo->query('SELECT ' . implode(',', $fields) . '  FROM `server`.`users` WHERE status = 1');

		$principals = [];

		while ($row = $result->fetch(\PDO::FETCH_ASSOC))
		{
			list($rowPrefix) = Uri\split($row['uri']);
			if ($rowPrefix !== $prefixPath)
				continue;

			$principal = ['uri' => $row['uri']];
			foreach ($this->fieldMap as $key => $value)
				if ($row[$value['dbField']])
					$principal[$key] = $row[$value['dbField']];
			$principals[] = $principal;
		}
		return $principals;
	}

	function getPrincipalByPath($path)
	{

		$fields = ['id', 'admin', 'CONCAT("principals/",email) as uri'];

		foreach ($this->fieldMap as $key => $value)
		$fields[] = $value['dbField'];

		$stmt = $this->pdo->prepare('SELECT ' . implode(',', $fields) . '  FROM `server`.`users` WHERE CONCAT("principals/",email) = ?');
		$stmt->execute([$path]);

		$row = $stmt->fetch(\PDO::FETCH_ASSOC);
		if (!$row)
			return;

		$principal = ['id'  => $row['id'], 'uri' => $row['uri'], 'admin' => $row['admin']];
		foreach ($this->fieldMap as $key => $value)
			if ($row[$value['dbField']])
				$principal[$key] = $row[$value['dbField']];
		
		return $principal;
	}


	function updatePrincipal($path, DAV\PropPatch $propPatch)
	{
		return true;
	}


	function searchPrincipals($prefixPath, array $searchProperties, $test = 'allof')
	{
		if (count($searchProperties) == 0)
			return [];

		$query = 'SELECT CONCAT("principals/",email) as uri FROM `server`.`users` WHERE ';
		$values = [];
		foreach ($searchProperties as $property => $value)
		{
			switch ($property)
			{
				case '{DAV:}displayname' :
					$column = "email";
				break;
				case '{http://sabredav.org/ns}email-address' :
					$column = "email";
				break;
				default :
					// Unsupported property
				return [];
			}
			if (count($values) > 0)
				$query .= (strcmp($test, "anyof") == 0 ? " OR " : " AND ");
			$query .= 'lower(' . $column . ') LIKE lower(?)';
			$values[] = '%' . $value . '%';
		}

		$stmt = $this->pdo->prepare($query);
		$stmt->execute($values);

		$principals = [];
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
		{
			list($rowPrefix) = Uri\split($row['uri']);
			if ($rowPrefix !== $prefixPath)
				continue;
			$principals[] = $row['uri'];
		}

		return $principals;

	}


	function findByUri($uri, $principalPrefix)
	{
		$value = null;
		$scheme = null;
		list($scheme, $value) = explode(":", $uri, 2);
		if (empty($value))
			return null;

		$uri = null;
		switch ($scheme)
		{
			case "mailto":
				$query = 'SELECT CONCAT("principals/",email) as uri FROM `server`.`users` WHERE lower(email)=lower(?)';
				$stmt = $this->pdo->prepare($query);
				$stmt->execute([ $value ]);

				while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
				{
					list($rowPrefix) = Uri\split($row['uri']);
					if ($rowPrefix !== $principalPrefix)
						continue;

					$uri = $row['uri'];
					break;
				}
			break;
			default:
				//unsupported uri scheme
			return null;
		}
		return $uri;
	}


	function getGroupMemberSet($principal)
	{
		$principal = $this->getPrincipalByPath($principal);
		if (!$principal) 
			throw new DAV\Exception('Principal not found');

		$stmt = $this->pdo->prepare("SELECT CONCAT('principals/',email) as uri FROM `server`.`users`, `server`.`authorizations` WHERE `users`.id = `authorizations`.user AND `authorizations`.resource = 'allfiles' AND `authorizations`.owner = ?");
		$stmt->execute([$principal['id']]);

		$result = [];
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
			$result[] = $row['uri'];

		return $result;
	}


	function getGroupMembership($principal)
	{
		static $result;

		if ($result == null)
		{
			$principal = $this->getPrincipalByPath($principal);
			if (!$principal)
				throw new DAV\Exception('Principal not found');

			$result = [];
			
			if ($principal['admin'] == 1)
				$stmt = $this->pdo->prepare("SELECT CONCAT('principals/',email) as uri FROM `server`.`users`");
			else
				$stmt = $this->pdo->prepare("SELECT CONCAT('principals/',email) as uri FROM `server`.`users`, `server`.`authorizations` WHERE `users`.id = `authorizations`.owner AND `authorizations`.resource = 'allfiles' AND `authorizations`.user = ?");
			$stmt->execute([$principal['id']]);
			while ($row = $stmt->fetch(\PDO::FETCH_ASSOC))
				$result[] = $row['uri'];

			if ($principal['admin'] == 0)
				$result[] = $principal['uri'];
		}
		return $result;
	}


	function setGroupMemberSet($principal, array $members)
	{
		return true;
	}

}
