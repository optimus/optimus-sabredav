FROM php:7.4-fpm

RUN apt-get update && apt-get install -y -qq \
	cron \
	git \
	libzip-dev \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-install pdo_mysql pcntl zip

RUN mkdir -p /srv/init && mkdir -p /srv/www \
	&& touch /usr/local/etc/php-fpm.d/logging.conf

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&& php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
	&& php -r "unlink('composer-setup.php');"

COPY ./optimus-sabredav/php.ini /usr/local/etc/php/conf.d

COPY ./optimus-libs/init.php /srv/init
COPY ./optimus-libs/JWT.php /srv/www/libs/JWT.php
COPY ./optimus-sabredav/www /srv/www
COPY ./optimus-sabredav/sql /srv/sql
COPY ./optimus-sabredav/vhosts/servers /srv/servers
COPY ./optimus-sabredav/vhosts/locations /srv/locations

WORKDIR /srv/www
RUN composer install --no-interaction --no-dev

RUN chown www-data /usr/local/etc/php/conf.d/php.ini \
	&& chown www-data /usr/local/etc/php-fpm.d/zz-docker.conf \
	&& chown www-data /usr/local/etc/php-fpm.d/logging.conf

ENTRYPOINT ["php", "/srv/init/init.php"]